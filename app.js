const app = require('express')();
const server = require('http').createServer(app);
const corsOptions = {
    origin: "*",
    optionsSuccessStatus: 200
  };
const io = require('socket.io')(server, {
    cors: {
        origin: "*",
        methods: ["PUT", "GET", "POST", "DELETE", "OPTIONS"],
        credentials: false,
      }
});
const port = process.env.PORT || 8080;
const cors = require('cors');
app.use(cors(corsOptions));
app.get('/', function(req, res) {
   res.json({status:'ok'});
});

io.on('connection', (socket) => {
    console.log('a user connected');
    socket.on('chat message', function (data) {
        console.log(data)
        io.emit('chat message', data);
    });
})
server.listen(port, function() {
  console.log(`Listening on port ${port}`);
});